import { App } from 'vue';
import components from './components';

export * from './components';
// 默认的完整引入方式
const install = (app: App) => {
  components.forEach((component) => {
    app.use(component);
  });
};
// 直接返回一个install函数
export default install;
