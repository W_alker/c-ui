import { ExtractPropTypes, PropType } from 'vue';

export const sliderProps = {
  value: Number,
  defaultValue: Number,
  max: Number,
  min: Number,
  steps: [Number, Array] as PropType<number | Array<number>>,
  marks: [Boolean, Object] as PropType<boolean | object>,
  tip: {
    type: Boolean,
    default: true,
  },
  disabled: Boolean,
} as const;

export type SliderProps = ExtractPropTypes<typeof sliderProps>;
