import _Slider from './src/slider.vue';
import { withInstall } from '@c-ui/utils/withInstall';

const Slider = withInstall(_Slider); // 生成带有 install 方法的组件

export default Slider; // 导出组件
export type { SliderProps } from './src/slider'; // 导出组件 props 的类型

declare module 'vue' {
  export interface GlobalComponents {
    CSlider: typeof Slider;
  }
}
