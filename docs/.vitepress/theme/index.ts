import DefaultTheme from 'vitepress/theme';
import '@c-ui/theme-chalk/src/index.scss';
import './index.scss';
import CUI from '@c-ui/components';

export default {
  ...DefaultTheme,
  enhanceApp({ app }) {
    app.use(CUI); // 注册组件
  },
};
