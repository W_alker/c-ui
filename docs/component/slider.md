# Slider

一个滑动选择器。

## 演示

### 基础用法

组件宽度可以自己调整。
<slider1></slider1>

```vue
<template>
  <div class="my-slider">
    <CSlider v-model:value="value" />
    <div class="btns">
      <button @click="value++">+</button>
      <span>{{ value }}</span>
      <button @click="value--">-</button>
    </div>
  </div>
</template>

<script lang="ts" setup>
import { ref } from 'vue';

const value = ref(30);
</script>

<style scoped lang="scss">
.my-slider {
  width: 500px;
}
</style>
```

### 最大值和最小值

`max` 接收一个数值作为最大值。默认 100 为最大值。<br/>
`min` 接收一个数值作为最小值。默认 0 为最小值。<br/>
`default-value`作为默认值，同时设置`value`的话该属性会失效。

<slider2 />

```vue
<template>
  <CSlider :max="600" :default-value="200" />

  <CSlider :max="650" :min="400" :default-value="200" />
</template>
```

### 设置步长

`steps` 接收一个`number`或者`Array`作为步长。
当然，不要输入一个意料之外的非递增数组。
<slider3 />

```vue
<template>
  <CSlider :value="350" :max="600" :min="200" :steps="150" :marks="true" />

  <CSlider :max="600" :steps="[0, 110, 340, 480, 600]" :marks="true" />
</template>
```

### 设置标记

正如上面演示的一样，一般情况下你只需要设置`marks`为 true，组件会根据你输入的`steps`打上标记。
当然你也可以直接或者额外设置一个`object`类型的`marks`。<br/>
需要注意的是不设置`steps`的话 slider 无法移动到尾部，这时候建议设置上`max`属性.。
<slider4 />

```vue
<template>
  <CSlider :value="300" :max="600" :marks="marks" />
  <CSlider :max="600" :steps="150" :marks="marks2" />
</template>

<script lang="ts" setup>
import { reactive } from 'vue';
const marks = reactive({
  60: 'S',
  120: 'L',
  180: 'I',
  258: 'D',
  320: 'E',
  460: 'R',
});

const marks2 = reactive({
  0: 'C',
  60: 'S',
  120: 'L',
  258: 'I',
  320: 'D',
  460: 'E',
  600: 'R',
});
</script>
```

### 关闭提示

不需要提示的话可以关闭。
<slider5 />

```vue
<template>
  <CSlider :tip="false" />
</template>
```

### 禁用

老规矩。
<slider6 />

```vue
<template>
  <CSlider :default-value="30" disabled />
</template>
```

### 插槽

如果不喜欢默认的滑块按钮，就自己定义一个吧。
<slider7 />

```vue
<template>
  <CSlider :value="30">
    <template #thumb>
      <span class="thumb"></span>
    </template>
  </CSlider>
</template>

<style lang="scss">
.thumb {
  display: block;
  width: 15px;
  height: 25px;
  background-color: var(--c-fill-color);
  border-radius: 3px;
}
</style>
```

## API

### Slider Props

| 名称          | 类型              | 默认值    | 说明         |
| ------------- | ----------------- | --------- | ------------ |
| default-value | number            | undefined | 初始值       |
| value         | number            | undefined | 值           |
| max           | number            | 100       | 最大值       |
| min           | number            | 0         | 最小值       |
| steps         | number \| array   | undefined | 步长         |
| marks         | boolean \| object | undefined | 标记         |
| tip           | boolean           | true      | 是否显示提示 |
| disabled      | boolean           | false     | 禁用         |

### Slider Events

| 名称       | 类型                     | 参数              | 说明                                          |
| ---------- | ------------------------ | ----------------- | --------------------------------------------- |
| touchstart | void                     | undefined         | 滑块按钮刚使用鼠标左键点击                    |
| touching   | (value: number) => \{ \} | slider 内部 value | 滑块按钮移动时，此时的值还没有传递到父组件    |
| touchend   | (value: number) => \{ \} | value             | 滑块移动结束，值更新                          |
| change     | (value: number) => \{ \} | value             | 值更新时触发，包括 touchend 和直接点击 slider |

### Slider Slots

| 名称  | 参数 | 说明           | 版本   |
| ----- | ---- | -------------- | ------ |
| thumb | ()   | 自定义滑块按钮 | 初始值 |

<script setup>
  import slider1 from '../vueComps/slider/slider1.vue'
  import slider2 from '../vueComps/slider/slider2.vue'
  import slider3 from '../vueComps/slider/slider3.vue'
  import slider4 from '../vueComps/slider/slider4.vue'
  import slider5 from '../vueComps/slider/slider5.vue'
  import slider6 from '../vueComps/slider/slider6.vue'
  import slider7 from '../vueComps/slider/slider7.vue'
</script>
