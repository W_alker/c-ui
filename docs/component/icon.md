# Icon 图标

z-ui 推荐使用 xicons 作为图标库。

```shell
$ pnpm install @vicons/ionicons5
```

## 使用图标

- 如果你想像用例一样直接使用，你需要全局注册组件，才能够直接在项目里使用。

```js
<script setup lang="ts">
import { AccessibilityOutline , ArrowRedoOutline} from '@vicons/ionicons5'
const handleClick = () =>{ alert(1)}
</script>

<CIcon color="#B1B2FF" size="40" @click="handleClick">
<AccessibilityOutline/>
</CIcon>

<CIcon color="#AAC4FF" size="40">
  <AccessibilityOutline/>
</CIcon>
<CIcon color="#D2DAFF" size="40">
  <AccessibilityOutline/>
</CIcon>
<div>

<CIcon color="#EBC7E8" size="60">
  <ArrowRedoOutline/>
</CIcon>

<CIcon color="#645CAA" size="60">
  <ArrowRedoOutline/>
</CIcon>

<CIcon color="#A084CA" size="60">
  <ArrowRedoOutline/>
</CIcon>
</div>

<script setup lang="ts">
import { CashOutline } from '@vicons/ionicons5';
</script>
<template>
  <CIcon color="red" size="40">
    <CashOutline />
  </CIcon>
</template>
```

## API

### Icon Props

| 名称  | 类型             | 默认值    | 说明     |
| ----- | ---------------- | --------- | -------- |
| color | string           | undefined | 图标颜色 |
| size  | number \| string | undefined | 图片大小 |
