import { createApp } from 'vue';
import './style.css';
import App from './App.vue';

import CUI from '@c-ui/components';
import '@c-ui/theme-chalk/src/index.scss';

import './assets/main.scss';

createApp(App).use(CUI).mount('#app');
